﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

	private GameObject player;
	private GameObject[] ghosts;

	// Start is called before the first frame update
	void Start()
    {
		player = GameObject.FindGameObjectWithTag("Player");
		ghosts = GameObject.FindGameObjectsWithTag("Ghost");

		player.transform.position = new Vector2(-15, Random.Range(0, 30));

		foreach (GameObject ghost in ghosts)
		{
			ghost.transform.position = new Vector2(Random.Range(-30, -15), Random.Range(0, 30));
		}
	}

    // Update is called once per frame
    void Update()
    {
		player.transform.position += Vector3.right * 30 * Time.deltaTime;

		if (player.transform.position.x > 40)
		{
			player.transform.position = new Vector2(-15, Random.Range(0, 30));
		}

		foreach (GameObject ghost in ghosts)
		{
			ghost.transform.position += Vector3.right * 30 * Time.deltaTime;

			if (ghost.transform.position.x > 40)
			{
				ghost.transform.position = new Vector2(Random.Range(-40, -15), Random.Range(0, 30));
			}

		}
	}
}
