﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinScene : MonoBehaviour
{

    public void Level1()
    {
        SceneManager.LoadScene("Level1");
    }

    public void Level2()
    {
        SceneManager.LoadScene("Level2");
    }


    public void Menu()
    {
        SceneManager.LoadScene("Menu");
    }


    public void Exit()
    {
        Application.Quit();
    }
}
