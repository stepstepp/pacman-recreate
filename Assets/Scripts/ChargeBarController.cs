﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChargeBarController : MonoBehaviour
{
    private Slider slider;
    public float fillSpeed = 1f;
    public float targetProgress = 0;

    public GameObject Pacman;
    private GameObject[] ghosts;

    private ParticleSystem particles;
    private TimerController timerController;

    public bool targetlock;

    // Start is called before the first frame update
    void Start()
    {
        Pacman = GameObject.FindGameObjectWithTag("Player");
        ghosts = GameObject.FindGameObjectsWithTag("Ghost");
        slider = GameObject.Find("Charge Bar").GetComponent<Slider>();
        particles = GameObject.Find("Particles").GetComponent<ParticleSystem>();
        timerController = GameObject.Find("GameControl").GetComponent<TimerController>();
    }

    // Update is called once per frame
    void Update()
    {
        //when space is pressed, consume the entire bar and put ghosts in scared mode
        if (Input.GetKeyDown(KeyCode.Space))
        {
            targetProgress = 0;
            targetlock = true;
            particles.Play();
           
            if (!slider.value.Equals(0))
            {
               foreach (GameObject ghost in ghosts)
                   ghost.GetComponent<Ghost>().Scare();
            }
        }

        //keeps consuming the bbar until the value is 0
        if (targetlock)
        {
            if (slider.value.Equals(0))
            {
                particles.Stop();

                foreach (GameObject ghost in ghosts)
                    ghost.GetComponent<Ghost>().ChaseMode();

                targetlock = false;
            }
        }

        //change colour of the bar to indicate it is full
        if (slider.value > .6)
        {
            GameObject.Find("Fill").GetComponent<Image>().color = new Color32(13, 145, 217,255);
            timerController.innerImage1.SetActive(true);
            timerController.innerImage2.SetActive(false);
        } else if (slider.value < .2)
        {
            GameObject.Find("Fill").GetComponent<Image>().color = Color.blue;
            if (!targetlock)
            {
                timerController.innerImage2.SetActive(true);
                timerController.innerImage1.SetActive(false);
            }
        } else
        {
            GameObject.Find("Fill").GetComponent<Image>().color = Color.blue;
            if (!targetlock)
            {
                timerController.innerImage2.SetActive(false);
                timerController.innerImage1.SetActive(false);
            }
           

        }

        UpdateBar();
    }

    public void UpdateBar()
    {
        //constantly balance the bar to its target value
        if (slider.value < targetProgress)
        {
            slider.value += fillSpeed * Time.deltaTime;
        }
        else if (slider.value > targetProgress)
        {
            slider.value -= fillSpeed * Time.deltaTime;
        }
    }
}
