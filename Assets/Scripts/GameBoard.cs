﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameBoard : MonoBehaviour
{
    //size of the gameboard
    public GameObject[,] board = new GameObject[28, 36];

    private GameObject player;
    private GameObject[] ghosts;

    public int totalPellets;
    public int consumedPellets;
    public int score = 0;
    
    private Text scoreText;
    private Text timeText;
    private float startTime;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        ghosts = GameObject.FindGameObjectsWithTag("Ghost");
        scoreText = GameObject.Find("Score").GetComponent<Text>();
        timeText = GameObject.Find("LevelTime").GetComponent<Text>();

        startTime = Time.time;
        Time.timeScale = 1f;

        Object[] objects = FindObjectsOfType(typeof(GameObject));

        //every object in the scene that has a tile script (aka pellets, power pellets and nodes) is given a x,y position

        foreach(GameObject o in objects)
        {
            if (o.tag != "Canvas")
            {

                Vector2 pos = o.transform.position;

                if (o.tag != "Player" && o.tag != "Ghost")
                {
                    //keep track of the total number of pellets to compare to number of consumed pellets

                    if (o.GetComponent<Tile>() != null && (o.GetComponent<Tile>().isPellet || o.GetComponent<Tile>().isPowerPellet))
                        totalPellets++;

                    board[(int)pos.x, (int)pos.y] = o;
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

        //sets the UI timer and score texts
        float t = Time.time - startTime;

        string minutes = ((int)t / 60).ToString();
        string seconds = (t % 60).ToString("f1");

        timeText.text = "Time - " + minutes + ":" + seconds;

        scoreText.text = "Score - " + (score * 100);

        //changes scene to victory scene when all pellets are collected
        if (consumedPellets == totalPellets)
            SceneManager.LoadScene("WinScene");

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("Menu");
        }
    }

    public void RestartLevel()
    {
        //deactivates the character sprites and restarts the scene after a delay

        player.SetActive(false);

        foreach (GameObject ghost in ghosts)
            ghost.SetActive(false);

        Invoke("RestartScene", .5f);
    }

    void RestartScene()
    {
        Scene currScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currScene.name);
    }
}
