﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ghost : MonoBehaviour
{
    public float speed = 5.9f;
    private float ghostReleaseTimer;
    private float scaredTimer;

    public AudioClip consumeGhost;
    public AudioClip ghostBlink;

    public bool isInGhostHouse;

    public GameObject player;
    private GameBoard gameControl;
    public static GameObject[,] board;
    
    public RuntimeAnimatorController scaredController;
    private RuntimeAnimatorController chaseAnimation;

    public Node GhostHouseNode;
    public Node ClockwiseNode1;
    public Node ClockwiseNode2;
    public Node ClockwiseNode3;
    public Node ClockwiseNode4;
    private Node targetNode;
    private Node prevNode;
    private Node currNode;

    private Vector2 direction;

    public enum Mode { Chase, Scared, Consumed }
    private Mode currMode;

    public enum GhostType { Red, Green, Pink, Blue }
    public GhostType ghostType;


    // Start is called before the first frame update
    void Start()
    {
        //set the global variable to be used later in the script.

        chaseAnimation = transform.GetComponent<Animator>().runtimeAnimatorController;
        gameControl = GameObject.Find("GameControl").GetComponent<GameBoard>();
        board = gameControl.GetComponent<GameBoard>().board;
        player = GameObject.FindGameObjectWithTag("Player");
        currNode = GetNode(transform.localPosition);

        //the ghosts are set to initially move to the left.

        direction = Vector2.left;
        targetNode = ChooseNextNode();
        prevNode = currNode;
    }

    // Update is called once per frame
    void Update()
    {
            ModeController();

            Move();

            ReleaseGhosts();

            CheckCollision();
    }

    void ReleaseGhosts()
    {
        GameObject tile = GetTile(transform.position);
        Node node = GetNode(transform.position);

        if (tile != null && tile.GetComponent<Tile>() != null && tile.GetComponent<Tile>().isGhostHouse)
        {
            if (node != null)
            {
                //release ghosts from the ghosthouses after a delay at the begining of the game.

                ghostReleaseTimer += Time.deltaTime;

                if (ghostReleaseTimer > 2.6f && isInGhostHouse)
                    isInGhostHouse = false;

                //sets the current nodes to the ghosthouse so that when a ghost returns to it after being consumed it recognises
                //which nodes are its neighbours. The direction is then set to up and mode is reverted back to chase.

                currNode = node;
                prevNode = currNode;
                targetNode = currNode.neighbours[0];
                direction = Vector2.up;

                currMode = Mode.Chase;
            }
        }
    }

    void CheckCollision()
    {
        //the player and ghosts each have invisible rect objects surrounding them which act like colliders when the rectangles overlap
        //if the player consumes the ghost while it is scared, it changes to consumed mode otherwise restarts the level.

        Rect ghostRect = new Rect(transform.position, transform.GetComponent<SpriteRenderer>().sprite.bounds.size / 4);
        Rect playerRect = new Rect(player.transform.position, transform.GetComponent<SpriteRenderer>().sprite.bounds.size / 4);

        if (ghostRect.Overlaps(playerRect))
        {
            if (currMode == Mode.Scared)
            {
                player.GetComponent<AudioSource>().PlayOneShot(consumeGhost);
                currMode = Mode.Consumed;
                gameControl.score += 10;
            }

            else if (currMode != Mode.Consumed)
                gameControl.RestartLevel();
        }
    }

    void ModeController()
    {
        //controls the different animators used, speeds and durations for each of the different modes.

        if (currMode == Mode.Chase)
        {
            transform.GetComponent<Animator>().runtimeAnimatorController = chaseAnimation;
            speed = 5.9f;
        }

        if (currMode == Mode.Scared)
        {
            transform.GetComponent<Animator>().runtimeAnimatorController = scaredController;
            speed = 2.9f;

            if (SceneManager.GetActiveScene().name != "Level2")
            {
                scaredTimer += Time.deltaTime;

                if (scaredTimer > 9f && scaredTimer < 9.05f)
                {
                    if (ghostBlink != null)
                        player.GetComponent<AudioSource>().PlayOneShot(ghostBlink);
                }

                if (scaredTimer >= 10.0f)
                    currMode = Mode.Chase;
            }
        }

        if (currMode == Mode.Consumed)
        {
            transform.GetComponent<Animator>().runtimeAnimatorController = chaseAnimation;
            speed = 15f;
        }
    }

    public void ScaredMode()
    {
        //called when the player picks up a power pellet, to change modes to scared and reset the timer if already in scared mode.

        if (currMode != Mode.Consumed)
        {
            currMode = Mode.Scared;
            scaredTimer = 0;
        }
    }

    public void Scare()
    {
        //called when the player picks up a power pellet, to change modes to scared and reset the timer if already in scared mode.

        if (currMode != Mode.Consumed)
        {
            currMode = Mode.Scared;
        }
    }

    public void ChaseMode()
    {
       // 
        if (currMode != Mode.Consumed)
        {
            currMode = Mode.Chase;
        }
    }


    void Move()
    {
        //movement allowed only if there is an available node to move to. Movement is continuous only in the facing direction and
        //continues even if the current node is passed by choosing the next available node to be the current node. The direction that
        //the ghost faces changes by flipping the sprite on its x-axis.

        if (targetNode != currNode && targetNode != null && !isInGhostHouse)
        {
            if (OverShotTarget())
            {
                currNode = targetNode;
                transform.localPosition = currNode.transform.position;

                targetNode = ChooseNextNode();
                prevNode = currNode;
                currNode = null;

                if (direction == Vector2.left)
                    transform.localScale = new Vector3(1, 1, 1);

                if (direction == Vector2.right)
                    transform.localScale = new Vector3(-1, 1, 1);
            } else
                transform.localPosition += (Vector3)direction * speed * Time.deltaTime;
        }
    }

    Vector2 GetTileToPlayer()
    {
        //returns a vector2 direction towards the player.

        Vector2 playerPosition = player.transform.localPosition;
        Vector2 targetTile = new Vector2(Mathf.RoundToInt(playerPosition.x), Mathf.RoundToInt(playerPosition.y));

        return targetTile;
    }

    Vector2 GetClockWiseTile()
    {
        //!!!!!does not currently work!!!!!//
        //returns a direction to a node in each of the corners of the map in a clockwise loop.

        Vector2 targetTile = ClockwiseNode1.transform.position;

            if (transform.position == ClockwiseNode1.transform.position)
                targetTile = ClockwiseNode2.transform.position;
            else if (transform.position == ClockwiseNode2.transform.position)
                targetTile = ClockwiseNode3.transform.position;
            else if (transform.position == ClockwiseNode3.transform.position)
                targetTile = ClockwiseNode4.transform.position;
            else if (transform.position == ClockwiseNode4.transform.position)
                targetTile = ClockwiseNode1.transform.position;

        return targetTile;
    }

    Node ChooseNextNode()
    {
        Vector2 targetTile = Vector2.zero;
        Vector2[] nodeDirections = new Vector2[4];

        Node moveToNode = null;
        Node[] foundNodes = new Node[4];

        int count = 0;

        if (currMode == Mode.Chase)
        {
            //in chase mode both the red and blue ghosts choose tiles in the direction of the player while the green ghost chooses
            //a random tile within the boundaries of the map. The pink ghost uses the 4 predetermined clockwise nodes as its directions
            //however it does not work at the moment.

            switch (ghostType)
            {
                case GhostType.Red:
                    targetTile = GetTileToPlayer();
                    break;
                case GhostType.Pink:
                    if (SceneManager.GetActiveScene().name != "Level2")
                        targetTile = GetTileToPlayer();
                    else
                        targetTile = new Vector2(Random.Range(1, 28), Random.Range(0, 36));
                    break;
                case GhostType.Green:
                    targetTile = new Vector2(Random.Range(1, 28), Random.Range(0, 36));
                    break;
                case GhostType.Blue:
                    targetTile = GetTileToPlayer();
                    break;
            }
        }
        else if (currMode == Mode.Scared)
        {
            //in scared mode the target tile of the ghosts are random.

            targetTile = new Vector2(Random.Range(1, 28), Random.Range(0, 36));
        }
        else if (currMode == Mode.Consumed)
        {
            //in consumed mode the target tile of the ghosts is back to its ghosthouse.

            targetTile = GhostHouseNode.transform.position;
        }

        //finds the number valid directions of the the current node and stores them in the foundNodes and validDirections arrays.
        //this is called at each level of the if statement so that the ghosts cannot move backwards AND ensures the ghosts make it
        //back to the ghosthouse in consumed mode AND does not allow movement from the ghosthouse entrance back into the ghosthouse.

        for (int i = 0; i < currNode.neighbours.Length; i++)
        {
            if (currNode.validDirections[i] != direction * -1)
            {
                if (currMode != Mode.Consumed)
                {
                    if (GetTile(currNode.transform.position).transform.GetComponent<Tile>().isGhostHouseEntrance == true)
                    {
                        if (currNode.validDirections[i] != Vector2.down)
                        {
                            foundNodes[count] = currNode.neighbours[i];
                            nodeDirections[count] = currNode.validDirections[i];
                            count++;
                        }
                    } else
                    {
                        foundNodes[count] = currNode.neighbours[i];
                        nodeDirections[count] = currNode.validDirections[i];
                        count++;
                    }
                } else
                {
                    foundNodes[count] = currNode.neighbours[i];
                    nodeDirections[count] = currNode.validDirections[i];
                    count++;
                }
            }
        }

        //if there is only one available direction it becomes the target node.

        if (foundNodes.Length == 1)
        {
            moveToNode = foundNodes[0];
            direction = nodeDirections[0];
        }

        //if there is more than one available direction, the found available node direction that leads to the shortest distance
        //to the target tile is chosen.

        if (foundNodes.Length > 1)
        {
            float leastDistance = 100000f;
            float greatestDistance = 0f;

            for (int i = 0; i < foundNodes.Length; i++)
            {
                if (nodeDirections[i] != Vector2.zero)
                {
                    float distance = GetDistance(foundNodes[i].transform.position, targetTile);

                        if (ghostType == GhostType.Blue && SceneManager.GetActiveScene().name != "Level2")
                        {
                        //the blue ghost's target tile is the player and will instead choose a direction that leads to the greatest
                        //distance from it when in chase mode.
                        if (currMode != Mode.Consumed)
                        {
                            if (direction == Vector2.zero)
                                direction = nodeDirections[1];

                            if (distance > greatestDistance)
                            {
                                greatestDistance = distance;
                                moveToNode = foundNodes[i];
                                direction = nodeDirections[i];
                            }
                        } else
                        {
                            //in consumed mode the blue ghost finds the fastest direction to the ghosthouse node.

                            leastDistance = distance;
                            moveToNode = foundNodes[i];
                            direction = nodeDirections[i];
                        }
                    } else if (distance < leastDistance)
                        {
                            leastDistance = distance;
                            moveToNode = foundNodes[i];
                            direction = nodeDirections[i];
                        }
                    }
                }
            }

        return moveToNode;
    }

    Node GetNode(Vector2 position)
    {
        //finds the node of the tile in ghosts' current position. otherwise returns null.

        GameObject tile = board[(int)position.x, (int)position.y];

        if (tile != null)
            return tile.GetComponent<Node>();

        return null;
    }

    GameObject GetTile(Vector2 position)
    {
        //finds the tile at the ghosts' current position.

        GameObject tile = board[Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y)];

        if (tile != null)
            return tile;

        return null;
    }

    bool OverShotTarget()
    {
        //returns true if the ghost has passed its target node. 

        return LengthFromNode(transform.localPosition) > LengthFromNode(targetNode.transform.position);
    }

    float LengthFromNode(Vector2 targetPosition)
    {
        //returns the length of the target tile from the previous node.

        Vector2 vec = targetPosition - (Vector2) prevNode.transform.position;
        return vec.sqrMagnitude;
    }

    float GetDistance(Vector2 posA, Vector2 posB)
    {
        //returns the distance between the ghost and player.

        float dx = posA.x - posB.x;
        float dy = posA.y - posB.y;

        return Mathf.Sqrt(dx * dx + dy * dy);
    }
}
