﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
    public Node[] neighbours;
    public Vector2[] validDirections;

    // Start is called before the first frame update
    void Start()
    {
        //Calculates the direction of each neighbour node from itself for the pacman script to understand the valid directions

        validDirections = new Vector2[neighbours.Length];

        for (int i = 0; i< neighbours.Length; i++)
        {
            validDirections[i] = (neighbours[i].transform.localPosition - transform.localPosition).normalized;
        }
    }
}
