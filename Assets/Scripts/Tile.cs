﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    public bool isPellet;
    public bool isPowerPellet;
    public bool didConsume;
    public bool isGhostHouse;
    public bool isGhostHouseEntrance;
}
