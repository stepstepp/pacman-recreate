﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerController : MonoBehaviour
{
    public Transform timerFill;

    [SerializeField]
    private float speed = 25;

    [SerializeField]
    private float currentAmt;

    public GameObject Pacman;
    private Slider slider;
    private GameObject speedParticles;
    private GameObject[] ghosts;

    public GameObject innerImage1;
    public GameObject innerImage2;

    void Start()
    {
        Pacman = GameObject.FindGameObjectWithTag("Player");
        speedParticles = GameObject.Find("Speed Particles");
        slider = GameObject.Find("Charge Bar").GetComponent<Slider>();
        ghosts = GameObject.FindGameObjectsWithTag("Ghost");

        innerImage1 = GameObject.Find("InnerImage1");
        innerImage2 = GameObject.Find("InnerImage2");

    }

    // Update is called once per frame
    void Update()
    {
        //loops the timer and resets once full. 
        timerFill.GetComponent<Image>().fillAmount = currentAmt / 100;

        if (currentAmt < 100)
        {
            currentAmt += speed * Time.deltaTime;

            if (currentAmt > 95)
            {
                timerFill.GetComponent<Image>().color = Color.red;
            }
        }
        else
        {
            currentAmt = 0;
            timerFill.GetComponent<Image>().color = Color.blue;

            if (slider.value > .6)
            {
                Pacman.GetComponent<Pacman>().speed = 8f;
                speedParticles.GetComponent<ParticleSystem>().Play();

            } else if (slider.value < .2)
            {
                Pacman.GetComponent<Pacman>().speed = 4.5f;
                speedParticles.GetComponent<ParticleSystem>().Stop();
            } else
            {

                Pacman.GetComponent<Pacman>().speed = 6f;
                speedParticles.GetComponent<ParticleSystem>().Stop();
            }
        }
    }
}
