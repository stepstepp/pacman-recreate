﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Pacman : MonoBehaviour
{
    public float speed = 6.0f;

    public AudioClip chomp;
    public AudioClip superPellet;
    private AudioSource audioSource;

    private GameObject[] ghosts;
    private GameBoard gameControl;
    public static GameObject[,] board;

    private Vector2 direction = Vector2.zero;
    private Vector2 nextDirection;

    private Node currNode;
    private Node prevNode;
    private Node targetNode;


    // Start is called before the first frame update
    void Start()
    {
        //set the global variable to be used later in the script.
        ghosts = GameObject.FindGameObjectsWithTag("Ghost");
        gameControl = GameObject.Find("GameControl").GetComponent<GameBoard>();
        audioSource = transform.GetComponent<AudioSource>();
        board = gameControl.GetComponent<GameBoard>().board;
        currNode = GetNode(transform.localPosition);

        //set the player to initially move left
        ChangeDirection(Vector2.left);
    }

    // Update is called once per frame
    void Update()
    {
            CheckInput();

            Move();

            UpdateOrientation();

            ConsumePellet();
    }

    void CheckInput()
    {
        //uses the arrowkeys and WASD keys as input to move the player

        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
            ChangeDirection(Vector2.right);
        else if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
            ChangeDirection(Vector2.left);
        else if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
            ChangeDirection(Vector2.up);
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
            ChangeDirection(Vector2.down);

    }

    void ChangeDirection (Vector2 newDirection)
    {
        //moves the player in a new direction only when it reaches the next node, then sets the directions and target nodes to the
        //new node

        if (newDirection != direction)
            nextDirection = newDirection;

        if (currNode != null)
        {
            Node newNode = ValidMove(newDirection);

            if (newNode != null)
            {
                direction = newDirection;
                targetNode = newNode;
                prevNode = currNode;
                currNode = null;
            }
        }
    }

    void Move()
    {
        //moves the player continuously between nodes only if there is an available node to move to.

        if (targetNode != currNode && targetNode != null)
        {
            //reverses the direction of movement
            if (nextDirection == direction * -1)
            {
                direction *= -1;
                Node tempNode = targetNode;
                targetNode = prevNode;
                prevNode = tempNode;
            }

            if (OverShotTarget())
            {
                //if there is a further node in the same direction of movement, it becomes the new target node otherwise stops
                //the player from moving further than the target node by completely stopping or changing direction

                currNode = targetNode;
                transform.localPosition = currNode.transform.position;

                Node newNode = ValidMove(nextDirection);
                if (newNode != null)
                {
                    direction = nextDirection;
                }

                if (newNode == null)
                    newNode = ValidMove(direction);

                if(newNode != null)
                {
                    targetNode = newNode;
                    prevNode = currNode;
                    currNode = null;
                } else
                    direction = Vector2.zero;
            } else
                transform.localPosition += (Vector3)direction * speed * Time.deltaTime;
        }  
    }

    void MoveToNode(Vector2 dir)
    {
        //changes the current position and current node to the next target node
        Node moveToNode = ValidMove(dir);

        if (moveToNode != null)
        {
            transform.localPosition = moveToNode.transform.localPosition;
            currNode = moveToNode;
        }
    }


    void UpdateOrientation()
    {
        //changes the orientation of the player depending on the direction it is facing

        if (direction == Vector2.left)
        {
            transform.localScale = new Vector3(-2, 2, 2);
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else if (direction == Vector2.right)
        {
            transform.localScale = new Vector3(2, 2, 2);
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else if (direction == Vector2.up)
        {
            transform.localScale = new Vector3(2, 2, 2);
            transform.rotation = Quaternion.Euler(0, 0, 90);
        }
        else if (direction == Vector2.down)
        {
            transform.localScale = new Vector3(2, 2, 2);
            transform.rotation = Quaternion.Euler(0, 0, 270);
        }
    }

    Node ValidMove(Vector2 dir)
    {
        //returns the neighbouring node in the specified direction from the current node

        Node moveToNode = null;

        for (int i = 0; i < currNode.neighbours.Length; i++)
        {
            if (currNode.validDirections [i] == dir)
            {
                moveToNode = currNode.neighbours[i];
                break;
            }
        }
        return moveToNode;
    }

    void ConsumePellet()
    {
        //gets the current tile and if it is a pellet, it consumes it by removing it from view and adding it to the consumed pellets
        //counter. If it's a power pellet, initiate scared mode on each of the ghosts.

        GameObject obj = GetTile(transform.position);

        if (obj!= null)
        {
            Tile tile = obj.GetComponent<Tile>();

                if (tile != null && !tile.didConsume && (tile.isPellet || tile.isPowerPellet))
                {
                    audioSource.PlayOneShot(chomp);
                    obj.GetComponent<SpriteRenderer>().enabled = false;
                    tile.didConsume = true;
                    gameControl.consumedPellets += 1;
                    gameControl.score += 1;

                if (SceneManager.GetActiveScene().name == "Level2")
                {
                    if (!gameControl.GetComponent<ChargeBarController>().targetlock)
                    gameControl.GetComponent<ChargeBarController>().targetProgress += 0.05f;
                }

                    if (tile.isPowerPellet)
                    {
                        audioSource.PlayOneShot(superPellet);

                        foreach (GameObject ghost in ghosts)
                            ghost.GetComponent<Ghost>().ScaredMode();
                }
            }
        }
    }

    GameObject GetTile (Vector2 position)
    {
        //finds the tile at the players' current position.

        GameObject tile = board[Mathf.RoundToInt(position.x), Mathf.RoundToInt(position.y)];

        if (tile != null)
            return tile;

        return null;
    }

    Node GetNode(Vector2 position)
    {
        //finds the node of the tile in players' current position. otherwise returns null.

        GameObject tile = board[(int)position.x, (int)position.y];

        if (tile != null)
            return tile.GetComponent<Node>();

        return null;
    }

    bool OverShotTarget()
    {
        //returns true if the player has passed its target node. 

        return LengthFromNode(transform.localPosition) > LengthFromNode(targetNode.transform.position);
    }

    float LengthFromNode(Vector2 targetPosition)
    {
        //returns the length of the target tile from the previous node.

        Vector2 vec = targetPosition - (Vector2)prevNode.transform.position;
        return vec.sqrMagnitude;
    }
}
